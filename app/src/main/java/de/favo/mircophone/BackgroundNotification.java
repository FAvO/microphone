package de.favo.mircophone;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

class BackgroundNotification {

    private static final String NOTIFICATION_TAG = "Background";
    private static final String CHANNEL_ID = "BAckground_ID";

    private static void createNotificationChannel(Context cxt) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = cxt.getString(R.string.text_channel_title);
            String description = cxt.getString(R.string.text_channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = cxt.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }


    static void notify(final Service context) {
        final Resources res = context.getResources();

        final String title = res.getString(R.string.background_notification_title);
        final String text = res.getString(R.string.background_notification_text);

        createNotificationChannel(context);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)

                // Set required fields, including the small icon, the
                // notification title, and text.
                .setSmallIcon(R.drawable.ic_stat_background)
                .setContentTitle(title)
                .setContentText(text)

                // Use a default priority (recognized on devices running Android
                // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                // Set ticker text (preview) information for this notification.
                .setTicker(title)

                // Show a number. This is useful when stacking notifications of
                // a single type.
                .setNumber(0)

                // Set the pending intent to be initiated when the user touches
                // the notification.
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(context.getApplicationContext(),Microphone.class),
                                PendingIntent.FLAG_UPDATE_CURRENT))
                .setOngoing(true)
                .setShowWhen(false);

        notify(context, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Service context, final Notification notification) {
        context.startForeground(NOTIFICATION_TAG.hashCode(), notification);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    static void cancel(final Service context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null) {
            nm.cancel(NOTIFICATION_TAG, 0);
        }
        context.stopForeground(true);
    }
}
