package de.favo.mircophone;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

/**
 *     Microphone
 *     Copyright (C) 2019 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class LegalActivity extends Activity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);
    }

        public void onFinish(View v){
            SharedPreferences.Editor mSharedPreferencesEditor =
                    getSharedPreferences(Microphone.SHARED_PREFERENCES,MODE_PRIVATE).edit();
            mSharedPreferencesEditor.putBoolean(Microphone.SP_FIRST_TIME, false);
            mSharedPreferencesEditor.apply();
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((TextView)findViewById(R.id.textView9)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
