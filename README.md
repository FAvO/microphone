[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Android](https://img.shields.io/badge/designed%20for-Android-green.svg?logo=android&color=a4c639)](https://www.android.com)
[![Java](https://img.shields.io/badge/using-Java-blue.svg?logo=java&color=007396)](http://www.oracle.com/technetwork/java/index.html)

# Microphone
A simple microphone app for Android.

Here you are able to download the app:
[Google Play](https://play.google.com/store/apps/details?id=de.favo.mircophone)